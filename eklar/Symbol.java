package eklar;

import eklar.nodes.Expr;

public class Symbol {
    public static final int VAR = 1;
    public static final int FUNCTION = 1;
    public String name;
    public eklar.EType type;
    public int idx;
    public Env scope;
    public Expr exp;
    public int kind;

    public Symbol(String name, eklar.EType type) {
        this.name = name;
        this.type = type;
        this.kind = Symbol.VAR;
    }

    public String toString() {
        return name + ":" + type;
    }

    public String getDescriptor() {
        return type.getDescriptor();
    }
}
