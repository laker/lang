package eklar;
import java.util.List;

import eklar.nodes.Expr;
import eklar.nodes.Expr.Param;

public class FunctionSymbol extends Symbol {
    public Expr exp;
    public List<Param> args;
    public FunctionSymbol(String name, eklar.EType type, List<Param> args, Expr exp) {
        super(name, type);
        this.args = args;
        this.exp = exp;
        this.kind = Symbol.FUNCTION;
    }

    public String getDescriptor() {
/*        String t = "(";
        if (args != null) {
            for (int i = 0; i < args.size(); i++) {
                t += args.get(i).type.getDescriptor();
            }
            t.substring(0, t.length() - 1);
        }
        t += ")";

        String s = t + type.getDescriptor();
*/
//    	above is not needed anymore?!
        return type.getDescriptor();
    }
}
