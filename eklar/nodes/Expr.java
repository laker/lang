package eklar.nodes;
import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;

import eklar.EType;
import eklar.Env;
import eklar.Symbol;
import eklar.Visitor;

public class Expr extends CommonTree {
    public eklar.EType type;

    public Expr() {
        super();
    }

    public Expr(Token token) {
        super(token);
    }

    public <T> T accept(Visitor<T> v) {
        return null;
    }

    public String getDescriptor() {
        return null;
    }

    public String toString() {
        return super.toString();
        //        return token.getText() + "<" + this.getClass().getName() + ">";
    }

    public static class ExprList extends Expr {
        public ExprList(int ttype) { super(new CommonToken(ttype, "RootNode")); }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class IfExpr extends Expr {
        public Expr cond;
        public Expr thenexpr;
        public Expr elseexpr;

        public IfExpr(int tt, Token token, Expr ife, Expr t1, Expr t2) {
            super(token);
            cond = ife;
            thenexpr = t1;
            elseexpr = t2;
        }

        public String toString() {
            return "IfExpr=(" + token.getText() + " " + cond + " " + thenexpr + " " + elseexpr + ")";
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class ForExpr extends Expr {
        public Expr e1;
        public Expr e2;
        public Expr e3;

        public ForExpr(int i, Token t, Expr e1, Expr e2, Expr e3) {
            super(t);
            this.e1 = e1;
            this.e2 = e2;
            this.e3 = e3;
        }

        public String toString() {
            return "(ForExpr:"
                + " " +  token.getText()
                + " " + e1
                + " " + e2
                + " " + e3
                + ")";
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class NumExpr extends Expr {
        public NumExpr(Token t) {
            token = t;
        }
        
        @Override
        public String toString() { return "NumExpr=" + token.getText(); }

        @Override
        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class FloatExpr extends Expr {
        public FloatExpr(Token t) {
            token = t;
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class StringExpr extends Expr {
        public StringExpr(Token t) {
            token = t;
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class CharExpr extends Expr {
        public CharExpr(Token t) {
            token = t;
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class Identifier extends Expr {
        public Symbol sym;
        public Identifier(Token t) {
            super(t);
        }

        @Override
        public String toString() {
            return "Identifier=" + token.getText();
        }

        @Override
        public String getDescriptor() {
            Symbol s = sym.scope.get(token.getText());
            return s.exp.getDescriptor();
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class LetExpr extends Expr {
        public Var binding;
        public Expr exp;

        public LetExpr(Var binding, Expr exp) {
            this.binding = binding;
            this.exp = exp;
        }

        public String toString() {
            return "(LetExpr=" + binding + " " + exp + ")";
        }


        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class ListExpr extends Expr {
        public ListExpr(Token t) {
            token = t;
        }

        public ListExpr(int ttype) { super(new CommonToken(ttype, "[...]")); }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class Func extends Expr {
        public final Expr exp;
        public final List<Param> params;
        public final String name;
        public Symbol sym;

        public Func(int i, Token t, eklar.EType retType, Expr exp, List<Param> params) {
            super(t);
            this.name = t.getText();
            this.type = retType;
            this.exp = exp;
            this.params = params;
        }

        @Override
        public String toString() {
            return "Func=(" + token.getText() + "(" + params + ")" + ":" + type + " " + exp + ')';
        }

        @Override
        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }

        @Override
        public String getDescriptor() {
            String t = "(";
            if (params != null) {
                for (int i = 0; i < params.size(); i++) {
                    t += params.get(i).type.getDescriptor();
                }
                t.substring(0, t.length() - 1);
            }

            t += ")";

            String s = t + type.getDescriptor();
            return s;   
        }
    }

    public static class Param extends Expr {
        public String name;
        public eklar.EType type;
        public Env scope;

        public Param(int i, Token t, eklar.EType type) {
            super(new CommonToken(i, "Param"));
            name = t.getText();
            this.type = type;
        }

        public String toString() {
            return "(Param=" + name + ":" + type + ")";
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class Println extends Expr {
        public Expr expr;

        public Println(Expr expr) {
            this.expr = expr;
        }

        @Override
        public String toString() { return "(Println " + expr + ")"; }

        @Override
        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class TupleExpr extends Expr {
        public TupleExpr(Token t) {
            token = t;
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class Var extends Expr {
        public String name;
        public eklar.EType type;
        public Expr exp;
        public Symbol sym;


        public Var(Token t, Expr exp) {
            name = t.getText();
            this.exp = exp;
        }

        public String toString() {
            return "(Var=" + name + ":" + EType.tagToType[type.tag] + ":" +  exp + ")";
        }


        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class ArrayExpr extends Expr {
        public ArrayExpr(Token t) {
            token = t;
        }

        public ArrayExpr(int ttype) { super(new CommonToken(ttype, "[|...|]")); }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class Binding extends Expr {
        public String name;
        public String type;
        public Expr expr;

        public Binding(int ttype) {
            super(new CommonToken(ttype, "Binding"));
        }

        public String toString() { return "Binding="; }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class Binop extends Expr {
    	public String op;
    	public Expr left;
    	public Expr right;
    	
        public Binop(int i) {
            super(new CommonToken(i, "binop"));
        }
        
        public Binop(String op, Expr left, Expr right) {
        	this.op = op;
        	this.left = left;
        	this.right = right;
        }

        public String toString() {
            return "(Binop=" + op + " " + left + " " + right + ")";
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class BoolExpr extends Expr {
        public BoolExpr(Token t) {
            token = t;
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }

    public static class Tuple extends Expr {
        public List<Expr> exprs;
        public Tuple(int i, Token t, List<Expr> exprs) {
            super(t);
            this.exprs = exprs;
        }

        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }

        @Override
        public String toString() {
            String s = "Tuple=( ";
            for (int i = 0; i < exprs.size(); i++)
                s += exprs.get(i) + " ";
            s += ")";
            return s;
        }
    }

    public static class Call extends Expr {
        public Expr exp;
        public List<Expr> args;

        public Call(Expr e1, List<Expr> e2) {
            exp = e1;
            args = e2;
        }

        @Override
        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }


        @Override
        public String getDescriptor() {
            String t = "(";

            for (int i = 0; i < args.size(); i++) {
                eklar.EType ty = args.get(i).type;
                t += ty.getDescriptor();
            }
            t.substring(0, t.length() - 1);
            t += ")";

            String s = t + type.getDescriptor();
            return s;
        }

        @Override
        public String toString() {
            return "CALL: " + exp + "(" + args + ")";
        }
    }

    public static class Match extends Expr {
        public List<Pattern> patterns;
        public Expr expr;
        public List<Expr> exprs;

        public Match(int i, Token t, Expr expr, List<Pattern> patterns, List<Expr> exprs) {
            super(t);
            this.expr = expr;
            this.patterns = patterns;
            this.exprs = exprs;
        }

        @Override
        public String toString() {
            StringBuffer buf = new StringBuffer();
            for (Expr e : patterns) buf.append(e + ",");
            return "Match=[" + buf + "]";
        }

        @Override
        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }

    }

    public static class Array extends Expr {
        public List<Expr> members;
        public Array(Token t, List<Expr> members) {
            super(t);
            this.members = members;
        }

        @Override
        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }

        @Override
        public String toString() {
            return "[| " + members + " |]";
        }
    }

    public static class ArrayGet extends Expr {
        public Expr array;
        public Expr idx;
        public ArrayGet(int i, Token t, Expr array, Expr idx) {
            super(t);
            this.array = array;
            this.idx = idx;
        }

        @Override
        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
        
        public String toString() {
            return "(ArrayGet " + array + ", " + idx + ")";
        }
    }
    
    public static class UnitExpr extends Expr {
    	public UnitExpr(Token t) {
    		super(t);
    	}
    	
    	@Override
        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
        
        public String toString() {
            return "UnitExpr";
        }
    }

    public static class ArraySet extends Expr {
        public Expr array;
        public Expr idx;
        public Expr val;
        public ArraySet(int i, Token t, Expr array, Expr idx, Expr val) {
            super(t);
            this.array = array;
            this.idx = idx;
            this.val = val;
        }

        @Override
        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
        
        public String toString() {
            return "ARRRAYSET";
        }
    }
    
    public static class ExprSeq extends Expr {
    	public final List<Expr> exprs;
    	
    	public ExprSeq(int i, List<Expr> exprs) {
    		this.exprs =  exprs;
    	}
    	
    	public EType type() {
    		return exprs.get(exprs.size() - 1).type;
    	}
    	
    	public String toString() {
    		return "EXPRSEQ";
    	}
    }

}
