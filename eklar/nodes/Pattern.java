package eklar.nodes;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.Token;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import eklar.EmitVisitor;


public abstract class Pattern extends Expr {
    public Expr expr;
    public Pattern() { super(); }
    public Pattern(Token t) { super(t); }
    public abstract void compile(MethodVisitor mv, EmitVisitor v, Expr e);

    public static class Const extends Pattern {
        public Const(int i, Expr expr) {
            super(new CommonToken(i, "Match"));
            this.expr = expr;
        }
        @Override
        public String getDescriptor() {
            return expr.getDescriptor();
        }

        @Override
        public String toString() {
            return "CONST(" + expr.token.getText() + ")";
        }
        
        @Override
        public void compile(MethodVisitor mv, EmitVisitor v, Expr e) {
            expr.accept(v);
            
        }
    }

    public static class Cons extends Pattern {
        Expr e1;
        Expr e2;
        
        public void compile(MethodVisitor mv, EmitVisitor v, Expr e) {}
        

        public Cons(int i, Token t, Expr e1, Expr e2) {
            super(t);
            this.e1 = e1;
            this.e2 = e2;
        }

        public String toString() {
            return "CONS(" + e1 + ", " + e2 + ")";
        }
    }
}
