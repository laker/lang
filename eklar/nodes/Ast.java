package eklar.nodes;

import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;

import eklar.Visitor;

public abstract class Ast extends CommonTree {
    public Ast() { super(); }
    public Ast(Token t) { super(t); }
    public abstract String getDescriptor();
    public abstract <T> T accept(Visitor<T> v);
}
