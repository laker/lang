package eklar.nodes;
import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;
import eklar.EType;
import eklar.Visitor;

public class Definition extends Expr {
    public Definition() {
        super();
    }

    public Definition(Token t) {
        super(t);
    }

    public static class RecordField extends Definition {
        public final String name;
        public final EType type;

        public RecordField(int i, Token name, EType type) {
            super(name);
            this.name = name.getText();
            this.type = type;
        }

        public String toString() {
            return name + ":" + type;
        }
    }

    public static class Record extends Definition {
        public final List<RecordField> fields;
        public Record(int i, Token t, List<RecordField> fields) {
            super(t);
            this.fields = fields;
        }

        public String toString() {
            return "(" + token.getText() + " " + fields + ")";
        }

        @Override
        public <T> T accept(Visitor<T> v) {
            return v.visit(this);
        }
    }
}
