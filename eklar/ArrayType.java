package eklar;

public class ArrayType extends EType {
    public ArrayType(EType type) {
        super(ARRAY_TAG, Object.class);
        this.type = type;
    }

    @Override
    public String getDescriptor() {
        return "Ljava/lang/Object;";
    }
}
