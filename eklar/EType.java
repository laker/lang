package eklar;
import org.objectweb.asm.Type;

import eklar.nodes.Expr.Tuple;

public class EType {
    public static final int ARRAY_TAG = 14;
    public static final int RECORD_TAG = 15;
    public static final EType INT = new EType(1, int.class);
    public static final EType VOID = new EType(2, void.class);
    public static final EType BOOL = new EType(3, boolean.class);
    public static final EType CHAR = new EType(4, char.class);
    public static final EType STRING = new EType(5, String.class);
    public static final EType UNIT = new EType(6, int.class);
    public static final EType FLOAT = new EType(8, float.class);
    public static final EType DOUBLE = new EType(9, double.class);
    public static final EType LONG = new EType(10, long.class);
    public static final EType SHORT = new EType(11, short.class);
    public static final EType BYTE = new EType(12, byte.class);
    public static final EType TUPLE = new EType(13, Tuple.class);

    public static final String tagToType[] = {
        "K", "int", "void", "boolean", "char", "string",
        "unit", "arrow", "float", "double",
        "long", "short", "byte", "tuple", "array",
        "record"
    };
    public int tag;
    public Class<?> clazz;
    public EType type;

    public static int width(EType t) {
        if (t.tag == 8 || t.tag == 10 || t.tag == 9)
            return 1;
        return 0;
    }

    public EType(int tag, Class<?> clazz) {
        this.tag = tag;
        this.clazz = clazz;
    }

    public String getDescriptor() {
        return org.objectweb.asm.Type.getDescriptor(clazz);
    }

    @Override
    public String toString() {
        return tagToType[tag];
    }

    public int getOpcode(int opcode) {
        Type ty = Type.getReturnType(getDescriptor());
        return ty.getOpcode(opcode);
    }

    // where
    //
    public static class Arrow extends EType {
        FunctionSymbol fs;
        public Arrow(FunctionSymbol fs) {
            super(6, null);
            this.fs = fs;
        }

        @Override
        public String getDescriptor() {
            return fs.getDescriptor();
        }
    }


    public static class Record extends EType {
        public Record() {
            super(RECORD_TAG, Object.class);
        }
    }
}
