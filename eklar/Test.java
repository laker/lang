package eklar;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.antlr.runtime.RecognitionException;


public class Test {
    public static void main(String[] args) throws RecognitionException,
            IOException, antlr.RecognitionException {
        InputStream input = null;
        if (args.length > 0)
            input = new FileInputStream(args[0]);
        else
            input = System.in;

        Compiler c = new Compiler();
        c.compile(input);
    }
}