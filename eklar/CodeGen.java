package eklar;
import java.io.FileOutputStream;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class CodeGen implements Opcodes {
    public ClassWriter cw;
    public FieldVisitor fv;
    public MethodVisitor mv;
    public AnnotationVisitor av0;

    public CodeGen() {
        cw = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
    }

    public void start() {
        cw.visit(Opcodes.V1_7, Opcodes.ACC_SUPER, "A", null, "java/lang/Object", null);
        mv = cw.visitMethod(0, "<init>", "()V", null, null);
        mv.visitCode();
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
        mv.visitInsn(Opcodes.RETURN);
        mv.visitMaxs(1, 1);
        mv.visitEnd();
    }

    public void end() {
        cw.visitEnd();

        byte b[] = cw.toByteArray();
        try {
            FileOutputStream fos = new FileOutputStream("A.class");
            fos.write(b);
            fos.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
