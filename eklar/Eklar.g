grammar Eklar;
options {
    output = AST;
    ASTLabelType = Expr;
    backtrack=true;
    memoize=true;
}

tokens {
    BINOP;
    EXPRLIST;
    LETEXPR;
    FOR='for';
    IF='if';
    LET='let';
    COLON=':';
    EQ='=';
    FUN='fn';
    PLUS='+';
    CALL;
    VARDEF;
    PRINTLN='println';
    MATCH='match';
    BINDING;
    PARAM;
    ARGS;
    TUPLE;
    PATTERN;
    CONS='::';
    ARRAY;
    ARRAYGET;
    ARRAYSET;
    RECORD;
    RECORDFIELD;
    EXPRSEQ;
}

@members {
    public Env env = new Env(null);
}
@parser::header { 
    package eklar; 
    import eklar.nodes.*;
    import eklar.nodes.Expr.*; 
    import eklar.nodes.Definition.*; 
    import static eklar.nodes.Pattern.*;
}
@lexer::header { package eklar; }

program returns [ArrayList<Expr> n]:
    (toplevel_phrase ';;'{ if ($n == null) $n = new ArrayList<Expr>(); $n.add($toplevel_phrase.n); })+ EOF 
    ;

toplevel_phrase returns [Expr n]: 
    expr { $n = $expr.n; }
    ;

forloop returns [Expr n]:
    a=FOR e1=letbinding ('to' | 'downto') e2=expr 'do' e3=expr 'done' { $n = new ForExpr(0, $a, $e1.n, $e2.n, $e3.n); }
    ;

parameters returns [List params]:
    a+=param (',' a+=param)*  { $params = $a; }
    ;

param:
    ID COLON type -> ^(PARAM<Param>[$ID, $type.t])
    ;

functionDefinition returns [Expr n]:
    LET name=ID ('(' parameters ')')? ':' type  '=' e=expr { $n = new Func(1, $name, $type.t, e.n, $parameters.params); }
    ;

arrayLiteral returns [Expr n]
scope { ArrayList<Expr> args; }
@init { $arrayLiteral::args = new ArrayList<Expr>(); }
:
    LAB (e=expr { $arrayLiteral::args.add($e.n); } (';' ee=expr {
    $arrayLiteral::args.add($ee.n); })* )? RAB { $n = new Expr.Array($LAB, $arrayLiteral::args); }
    ;

letbinding returns [Var n]:
    ID EQ expr { $n = new Var($ID, $expr.n); }
    ;

letExpression returns [Expr n]:
    LET letbinding 'in' e=expr { $n = new LetExpr($letbinding.n, $e.n); }
    ;

ifExpression returns [Expr n]:
    IF a=expr 'then' b=expr 'else' c=expr { $n = new IfExpr(1, $IF, $a.n, $b.n, $c.n); }
    ;

printLine returns [Expr n]:
    PRINTLN expr { $n = new Println($expr.n); }
    ;

expr returns [Expr n]:
    ifExpression { $n = $ifExpression.n; }
    | letExpression { $n = $letExpression.n; }
    | functionDefinition { $n = $functionDefinition.n; }
    | printLine { $n = $printLine.n; }
    | arrayLiteral { $n = $arrayLiteral.n; }
    | forloop { $n = $forloop.n; }
    | '(' ee=expr ')' { $n = $ee.n; }
    | atom { $n = $atom.n; } ( ('+' e=expr { $n = new Binop("+", $n, $e.n); })* 
                              
                             )?
    ;

type returns [EType t]:
    classOrInterfaceType('[' ']')* { $t = EType.INT; }
    | primitiveType('[' ']')* { $t = $primitiveType.t; }
    ;

classOrInterfaceType:
   ID (typeArguments)? ('.' ID (typeArguments)?)*
    ;

typeArguments:
   '<' typeArgument (',' typeArgument)* '>'
    ;

typeArgument:
    type
    |   '?'
        (
            ('extends'
            |'super'
            )
            type
        )?
    ;

primitiveType returns [EType t]
    :   'boolean' { $t = EType.BOOL; }
    |   'char' { $t = EType.CHAR; }
    |   'byte' { $t = EType.BYTE; }
    |   'short' { $t = EType.SHORT; }
    |   'int' { $t = EType.INT; }
    |   'long' { $t = EType.LONG; }
    |   'float' { $t = EType.FLOAT; }
    |   'double' { $t = EType.DOUBLE; }
    |   '()' { $t = EType.UNIT; }
    |   'String' { $t = EType.STRING; }
    ;

constant returns [Expr n]:
    INT { $n = new NumExpr($INT); }
   | FLOAT { $n = new FloatExpr($FLOAT); }
   | CHAR { $n = new CharExpr($CHAR); }
   | STRING { $n = new StringExpr($STRING); }
   | TRUE { $n = new BoolExpr($TRUE); }
   | FALSE { $n = new BoolExpr($FALSE); }
   | t='()' { $n = new UnitExpr($t); }
   ;

atom returns [Expr n]
scope {
    ArrayList<Expr> args;
}
@init { $atom::args = new ArrayList<Expr>(); }
:
   constant { $n = $constant.n; }
   | ID { $n = new Identifier($ID); } (
                                         '.(' e=expr ')' { $n = new ArrayGet(0,  null, $n, $e.n); }
                                        |  (e=expr { $atom::args.add($e.n); })+ { $n = new Call($n, $atom::args); }
                                      )?
   | '(' expr ')' { $n = $expr.n; }
  ;

UNDERSCORE: '_';

FATBAR: '|';

TRUE: 'true';

FALSE: 'false';

ID: LETTER (LETTER | '0' .. '9')* ;

fragment
LETTER: ('a' .. 'z' | 'A' ..'Z');

STRING: '\"' .* '\"' ;

INT :   '0'..'9'+ ;

FLOAT
    :   INT '.' INT*
    |   '.' INT+
    ;
LAB : '[|' ;
RAB : '|]' ;
LCB : '{' ;
RCB : '}' ;
DOT : '.' ;
CHAR: '\'' . '\'' ;

WS  :   (' '|'\t'|'\r'|'\n')+ {skip();} ;

SL_COMMENT
    :   '#' ~('\r'|'\n')* {$channel=HIDDEN;}
    ;
