package eklar;
import java.util.HashMap;

public class Env {
    HashMap<String, Symbol> symbols = new HashMap<String, Symbol>();
    public Env parent;

    public Env(Env e) {
        parent = e;
    }
    
    public Env push_scope() {
        Env e = new Env(this);
        return e;
    }
    
    public Env pop_scope() {
        return parent; 
    }
    
    public void put(Symbol s) {
        symbols.put(s.name, s);
    }

    public Symbol get(String name) {
        Symbol s = symbols.get(name);
        if (s != null)
            return s;

        if (parent != null)
            return parent.get(name);
        return null;
    }

    public String toString() {
        if (parent != null)
            return symbols.toString() + parent.symbols.toString();
        return symbols.toString();
    }
}
