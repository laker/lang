package eklar;
import java.io.IOException;
import java.io.InputStream;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenRewriteStream;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.TreeAdaptor;

import eklar.nodes.Expr;

public class Compiler {
    public static TreeAdaptor TreeAdaptor = new CommonTreeAdaptor() {
        public Object create(Token token) {
            return new Expr(token);
        }

        public Object dupNode(Object t) {
            if (t == null) {
                return null;
            }
            return create(((Expr) t).token);
        }

        public Object errorNode(TokenStream input, Token start, Token stop,
                RecognitionException e) {
            return new ErrorNode(input, start, stop, e);
        }
    };

    EklarParser p;
    
    public void compile(InputStream input) throws RecognitionException, IOException {
        EklarLexer lex = new EklarLexer(new ANTLRInputStream(input));
        TokenRewriteStream tokens = new TokenRewriteStream(lex);
        p = new EklarParser(tokens);
        p.setTreeAdaptor(TreeAdaptor);
        EklarParser.program_return r = p.program();

        if (p.getNumberOfSyntaxErrors() == 0) {
            //Expr root = (ExprList) r.getTree();
            //traverse(root, 0);
            TypeVisitor tv = new TypeVisitor(p.env);
            /*try {
               
                ((ExprList) root).accept(tv);
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
                return;
            }*/
            
            
            System.out.println("number of defs:"+r.n.size());
            for (Expr e : r.n) {
            	try {
            		e.accept(tv);
            	} catch (IllegalArgumentException ee) {
            		System.err.println(ee.getMessage());
            		return;
            	}
            }
        	System.out.println(r.n);
            
            //System.out.println("tree: "+root.toStringTree());

            try {
                CodeGen cg = new CodeGen();
                cg.start();

                EmitVisitor ev2 = new EmitVisitor(p.env, cg);
                for (Expr e : r.n)
                	e.accept(ev2);
                //((ExprList) root).accept(ev2);
                cg.end();
            } catch (ClassCastException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private static void traverse(CommonTree tree, int indent) {
        if (tree == null)
            return;

        for (int i = 0; i < indent; i++)
            System.out.print("  ");

        System.out.println(tree.getClass().getName() + " -> " + tree.getText());

        for (int i = 0; i < tree.getChildCount(); i++)
            traverse((CommonTree)tree.getChild(i), indent + 2);
    }

    public void error(String msg) {
        System.err.println(msg);
    }
}
