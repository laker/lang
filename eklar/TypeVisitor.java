package eklar;

import eklar.nodes.Expr.*;
import eklar.nodes.*;
import eklar.nodes.Expr.ArrayGet;
import eklar.nodes.Expr.ArraySet;
import eklar.nodes.Expr.ExprSeq;
import eklar.nodes.Expr.UnitExpr;

public class TypeVisitor implements Visitor<EType> {
    Env env;
    int idx;

    public TypeVisitor(Env env) {
        this.env = env;
    }

    @Override
    public EType visit(NumExpr n) {
        n.type = EType.INT;
        return EType.INT;
    }

    @Override
    public EType visit(Expr.ExprList n) {
        for (Object o : n.getChildren())
            ((Expr) o).accept(this);
        return null;
    }

    @Override
    public EType visit(Binop n) {
        EType t1 = n.left.accept(this);
        EType t2 = n.right.accept(this);
        
        if (!t1.equals(t2))
            error(n.token.getText() + " type error: " + t1 + "<>" + t2, n);

        if (n.op.equals("<")) {
            n.type = EType.BOOL;
            return EType.BOOL;
        }

        n.type = t1;
        return t1;
    }

    @Override
    public EType visit(FloatExpr n) {
        return EType.FLOAT;
    }

    @Override
    public EType visit(StringExpr n) {
    	n.type = EType.STRING;
        return n.type;
    }

    @Override
    public EType visit(CharExpr n) {
        return EType.CHAR;
    }

    @Override
    public EType visit(ArrayExpr n) {
        return null;
    }

    @Override
    public EType visit(ListExpr n) {
        return null;
    }

    @Override
    public EType visit(TupleExpr n) {
        return null;
    }

    @Override
    public EType visit(BoolExpr n) {
        return EType.BOOL;
    }

    @Override
    public EType visit(Call n) {
        String name = n.exp.token.getText();
        Symbol s = env.get(name);

        if (s == null)
            error("undefined function: " + name, n);

        if (s.kind != Symbol.FUNCTION)
            error(name + " is not a function", n);

        if (n.args.size() != ((FunctionSymbol)s).args.size())
            error("different number of arguments", n.exp);

        EType t = n.exp.accept(this);
        for (Expr e : n.args) e.accept(this);
        n.type = t;
        return t;
    }

    @Override
    public EType visit(Func n) {
        idx = 0;
        FunctionSymbol fs = new FunctionSymbol(n.name, n.type, n.params, n.exp);

        fs.scope = env;
        n.sym = fs;
        env.put(fs);
        env = env.push_scope();
        if (n.params != null)
            for (Param p : n.params) p.accept(this);
        n.exp.accept(this);

        env = env.pop_scope();
        n.type = new EType.Arrow(fs);
        return n.type;
    }

    @Override
    public EType visit(IfExpr n) {
        if (n.cond.accept(this).equals(EType.BOOL)) {
            EType t1 = n.thenexpr.accept(this);
            if (t1.equals(n.elseexpr.accept(this))) {
                n.type = t1;
                return t1;
            } else
                error("arms of conditional got different types", n);
        } else {
            error("condtional not a boolean", n);
        }
        return null;
    }

    @Override
    public EType visit(Identifier n) {
        String s = n.getText();
        Symbol sym = env.get(s);
        if (sym == null)
            error("Unbound value " + s, n);

        n.sym = sym;
        n.type = sym.type;
        return sym.type;
    }

    @Override
    public EType visit(Println n) {
        n.expr.accept(this);
        n.type = EType.UNIT;
        return n.type;
    }

    @Override
    public EType visit(ForExpr n) {
        EType t1 = n.e1.accept(this);
        EType t2 = n.e2.accept(this);
        EType t3 = n.e3.accept(this);

        if (!t1.equals(EType.INT))
            error("expected type int got: " + t1, n.e1);

        if (!t2.equals(EType.INT))
            error("expected type int got: " + t2, n.e2);

        if (!t3.equals(EType.UNIT))
            error("expected type () got: " + t3, n.e3);

        n.type = EType.UNIT;
        return n.type;
    }

    @Override
    public EType visit(Var n) {
        n.type = n.exp.accept(this);
        Symbol s = new VarSymbol(n.name, n.type);
        env.put(s);
        EType t2 = n.exp.accept(this);
        //if (!n.type.equals(t2))
        //error("trying to assign: " + t2 + " to " + n.type, n);
        s.scope = env;
        s.exp = n.exp;
        n.sym = s;
        return n.type;
    }

    @Override
    public EType visit(Binding n) {
        return null;
    }

    @Override
    public EType visit(Param n) {
        Symbol s = new VarSymbol(n.name, n.type);
        s.idx = idx;
        idx++;
        env.put(s);
        n.scope = env;
        return n.type;
    }

    public void error(String msg, Expr node) {
        throw new IllegalArgumentException("Error: " + msg + " at " + "line " +
                node.getLine() + ", " + "characters " 
                + node.getCharPositionInLine());
    }

    @Override
    public EType visit(LetExpr n) {
        env = env.push_scope();
        n.binding.accept(this);
        EType t = n.exp.accept(this);
        n.exp.type = t;
        env = env.pop_scope();
        return t;
    }

    @Override
    public EType visit(Tuple n) {
        n.type = EType.TUPLE;
        return n.type;
    }

    @Override
    public EType visit(Match n) {
        for (Expr e : n.exprs) e.accept(this);
        n.type = EType.INT;
        return n.type;
    }

    @Override
    public EType visit(Expr.Array n) {
        if (n.members.size() > 0) {
            EType t1 = n.members.get(0).accept(this);
            for (int i = 1; i < n.members.size(); i++) {
                EType t2 = n.members.get(i).accept(this);
                if (t1 != t2)
                    error("type error: got " + t2 + " expected: " + t1, n);
            }
            n.type = new ArrayType(t1);
        }

        return n.type;
    }

    @Override
    public EType visit(ArrayGet n) {
        EType t1 = n.array.accept(this);
        if (t1.tag != EType.ARRAY_TAG) {
            error("type erro: expected array, got: " + t1, n);
        }
        eklar.EType t2 = n.idx.accept(this);
        if (t2 != eklar.EType.INT) {
            error("type error: expected int, got: " + t2, n);
        }
        n.type = t2;
        return n.type;
    }

    @Override
    public EType visit(ArraySet n) {
        EType t1 = n.array.accept(this);
        EType t2 = n.idx.accept(this);
        EType t3 = n.val.accept(this);

        if (t1.tag != EType.ARRAY_TAG)
            error("type error: expected array, got: " + t1, n);
        if (t2 != eklar.EType.INT)
            error("type error: expected int, got: " + t2, n);

        EType t1a = (ArrayType) t1;

        if (t3.tag != t1a.type.tag)
            error("type error: cannot assign " + t3 + " to array of" + t1, n);
        n.type = EType.UNIT;
        return n.type;
    }

    @Override
    public EType visit(Definition.Record n) {
        n.type = new EType.Record();
        return n.type;
    }

	@Override
	public EType visit(UnitExpr n) {
		n.type = EType.UNIT;
		return n.type;
	}

	@Override
	public EType visit(ExprSeq n) {
		n.type = n.type();
		return n.type;
	}
}
