package eklar;

import java.io.FileOutputStream;
import java.util.ArrayList;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import eklar.nodes.Definition;
import eklar.nodes.Expr;
import eklar.nodes.Expr.ArrayExpr;
import eklar.nodes.Expr.ArrayGet;
import eklar.nodes.Expr.ArraySet;
import eklar.nodes.Expr.Binding;
import eklar.nodes.Expr.Binop;
import eklar.nodes.Expr.BoolExpr;
import eklar.nodes.Expr.Call;
import eklar.nodes.Expr.CharExpr;
import eklar.nodes.Expr.ExprList;
import eklar.nodes.Expr.ExprSeq;
import eklar.nodes.Expr.FloatExpr;
import eklar.nodes.Expr.ForExpr;
import eklar.nodes.Expr.Func;
import eklar.nodes.Expr.Identifier;
import eklar.nodes.Expr.IfExpr;
import eklar.nodes.Expr.LetExpr;
import eklar.nodes.Expr.ListExpr;
import eklar.nodes.Expr.Match;
import eklar.nodes.Expr.NumExpr;
import eklar.nodes.Expr.Param;
import eklar.nodes.Expr.Println;
import eklar.nodes.Expr.StringExpr;
import eklar.nodes.Expr.Tuple;
import eklar.nodes.Expr.TupleExpr;
import eklar.nodes.Expr.UnitExpr;
import eklar.nodes.Expr.Var;
import eklar.nodes.Pattern;

public class EmitVisitor implements Visitor<Void>, Opcodes {
    private CodeGen cg;
    int local;
    Env env;

    public EmitVisitor(Env env, CodeGen cg) {
        this.env = env;
        this.cg = cg;
        local = 1;
    }

    @Override
    public Void visit(NumExpr n) {
        int intval = Integer.parseInt(n.token.getText());
        pushInt(intval);
        return null;
    }

    @Override
    public Void visit(ExprList n) {
        for (Object o : n.getChildren())
            ((Expr) o).accept(this);
        return null;
    }

    @Override
    public Void visit(Binop n) {
        return binOp(n);
    }

    private Void binOp(Binop n) {
        Expr e1 = n.left;
        Expr e2 = n.right;

        e1.accept(this);
        e2.accept(this);

        String type = n.op;
        if (type.equals("+")) {
            cg.mv.visitInsn(Opcodes.IADD);
            return null;
        } else if (type.equals("-")) {
            cg.mv.visitInsn(Opcodes.ISUB);
            return null;
        } else if (type.equals("*")){
            cg.mv.visitInsn(Opcodes.IMUL);
            return null;
        } else if (type.equals("/")) {
            cg.mv.visitInsn(Opcodes.IDIV);
            return null;
        } else if (type.equals("<")) {
            Label l0 = new Label();
            cg.mv.visitJumpInsn(IF_ICMPGE, l0);
            cg.mv.visitInsn(ICONST_1);
            Label l1 = new Label();
            cg.mv.visitJumpInsn(GOTO, l1);
            cg.mv.visitLabel(l0);
            cg.mv.visitInsn(ICONST_0);
            cg.mv.visitLabel(l1);
            return null;
        } else
            throw new UnsupportedOperationException(type);
    }

    @Override
    public Void visit(FloatExpr n) {
        //return Double.parseDouble(n.token.getText());
        return null;
    }

    @Override
    public Void visit(StringExpr n) {
        cg.mv.visitLdcInsn(n.token.getText());
        return null;
    }

    @Override
    public Void visit(CharExpr n) {
        //return new Character(n.token.getText().charAt(1));
        return null;
    }

    @Override
    public Void visit(ArrayExpr n) {
        return null;
    }

    @Override
    public Void visit(ListExpr n) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Void visit(TupleExpr n) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Void visit(BoolExpr n) {
        if (n.token.getText().equals("true"))
            cg.mv.visitLdcInsn(new Integer(1));
        if (n.token.getText().equals("false"))
            cg.mv.visitLdcInsn(new Integer(0));
        return null;
    }

    @Override
    public Void visit(Call n) {
        for (Expr e : n.args) e.accept(this);
        //n.exp.accept(this);
        cg.mv.visitMethodInsn(INVOKESTATIC, "A", n.exp.token.getText(), n.getDescriptor());
        return null;
    }

    @Override
    public Void visit(Func n) {
        local = 1;
        String name = n.name;

        if (name.equals("main")) {
            cg.mv = cg.cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "main",
                    "([Ljava/lang/String;)V", null, null);
            cg.mv.visitCode();
            n.exp.accept(this);
            cg.mv.visitInsn(Opcodes.RETURN);
            cg.mv.visitMaxs(0, 0); // precomputed
            cg.mv.visitEnd();
        } else {
            cg.mv = cg.cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC + Opcodes.ACC_FINAL, name,
                    n.getDescriptor(), null, null);
            cg.mv.visitCode();

            n.exp.accept(this);
            Type ty = Type.getReturnType(n.getDescriptor());
            cg.mv.visitInsn(ty.getOpcode(Opcodes.IRETURN));
            cg.mv.visitMaxs(0, 0); // precomputed
            cg.mv.visitEnd();

        }
        return null;
    }

    @Override
    public Void visit(IfExpr n) {
        n.cond.accept(this);
        Label l0 = new Label();
        cg.mv.visitJumpInsn(Opcodes.IFEQ, l0);
        n.thenexpr.accept(this);
        Label l1 = new Label();
        cg.mv.visitJumpInsn(Opcodes.GOTO, l1);
        cg.mv.visitLabel(l0);
        n.elseexpr.accept(this);
        cg.mv.visitLabel(l1);
        return null;
    }

    @Override
    public Void visit(Identifier n) {
        if (n.sym instanceof FunctionSymbol) {
            cg.mv.visitMethodInsn(INVOKESTATIC, "A", n.token.getText(), n.getDescriptor());
        } else {
            Type ty = Type.getReturnType(n.sym.type.getDescriptor());
            cg.mv.visitVarInsn(ty.getOpcode(Opcodes.ILOAD), n.sym.idx);
        }
        return null;
    }

    @Override
    public Void visit(Println n) {
        Expr e = n.expr;
        cg.mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        e.accept(this);
        cg.mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(" + e.type.getDescriptor() + ")V");
        return null;
    }

    @Override
    public Void visit(ForExpr n) {
        n.e1.accept(this);
        Label l0 = new Label();
        cg.mv.visitLabel(l0);
        cg.mv.visitVarInsn(ILOAD, ((Var)n.e1).sym.idx);
        n.e2.accept(this);
        Label l1 = new Label();
        cg.mv.visitJumpInsn(IF_ICMPGT, l1);
        n.e3.accept(this);
        cg.mv.visitIincInsn(((Var)n.e1).sym.idx, 1);
        cg.mv.visitJumpInsn(GOTO, l0);
        cg.mv.visitLabel(l1);
        return null;
    }

    public Void visit(Var n) {
        n.exp.accept(this);
        Type ty = Type.getReturnType(n.sym.getDescriptor());
        cg.mv.visitVarInsn(ty.getOpcode(Opcodes.ISTORE), local);
        n.sym.idx = local;
        local++;
        return null;
    }

    public Void visit(Binding n) {
        return null;
    }


    @Override
    public Void visit(Param n) {
        return null;
    }

    @Override
    public Void visit(LetExpr n) {
        n.binding.accept(this);
        n.exp.accept(this);
        return null;
    }

    @Override
    public Void visit(Tuple n) {
        cg.mv.visitTypeInsn(NEW, "Tuple");
        cg.mv.visitInsn(DUP);
        for (Expr e : n.exprs) {
            e.accept(this);
            cg.mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer",
                    "valueOf", "(I)Ljava/lang/Integer;");
        }
        cg.mv.visitMethodInsn(INVOKESPECIAL, "Tuple", "<init>",
                "(Ljava/lang/Object;Ljava/lang/Object;)V");
        return null;
    }

    @Override
    public Void visit(Match n) {
        n.expr.accept(this);
        ArrayList<Label> labels = new ArrayList<Label>();


        ArrayList<Integer> opts = new ArrayList<Integer>();
        for (Pattern pat : n.patterns) {
            opts.add(new Integer(pat.expr.token.getText()));
            labels.add(new Label());
        }

        Label l0 = new Label();
        int[] cs = new int[opts.size()];
        for (int i = 0; i < opts.size(); i++) cs[i] = opts.get(i);

        cg.mv.visitLookupSwitchInsn(l0, cs, labels.toArray(new Label[labels.size()]));

        return null;
    }

    @Override
    public Void visit(Expr.Array n) {
        pushInt(n.members.size());
        cg.mv.visitIntInsn(Opcodes.NEWARRAY, Opcodes.T_INT);
        for (int i = 0; i < n.members.size(); i++) {
            cg.mv.visitInsn(Opcodes.DUP);
            pushInt(i);
            pushInt((new Integer(n.members.get(i).token.getText()).intValue()));
            cg.mv.visitInsn(Opcodes.IASTORE);
        }
        return null;
    }

    public void pushInt(int intval) {
        if (-1 <= intval && intval <= 5) {
            int op = 0;
            switch (intval) {
                case 0:
                    op = Opcodes.ICONST_0;
                    break;
                case 1:
                    op = Opcodes.ICONST_1;
                    break;
                case 2:
                    op = Opcodes.ICONST_2;
                    break;
                case 3:
                    op = Opcodes.ICONST_3;
                    break;
                case 4:
                    op = Opcodes.ICONST_4;
                    break;
                case 5:
                    op = Opcodes.ICONST_5;
                    break;
            }
            cg.mv.visitInsn(op);
        } else if (Byte.MIN_VALUE <= intval && intval <= Byte.MAX_VALUE)
            cg.mv.visitIntInsn(Opcodes.BIPUSH, intval);
        else if (Short.MIN_VALUE <= intval && intval <= Short.MAX_VALUE)
            cg.mv.visitIntInsn(Opcodes.SIPUSH, intval);
        else
            cg.mv.visitLdcInsn(intval);
    }

    @Override
    public Void visit(ArrayGet n) {
        n.array.accept(this);
        n.idx.accept(this);
        cg.mv.visitInsn(Opcodes.IALOAD);
        return null;
    }

    @Override
    public Void visit(ArraySet n) {
        n.array.accept(this);
        n.idx.accept(this);
        n.val.accept(this);
        cg.mv.visitInsn(Opcodes.IASTORE);
        //cg.mv.visitInsn(Opcodes.ICONST_0); // remove, temp hack for unit expr
        return null;
    }

    @Override
    public Void visit(Definition.Record n) {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
        FieldVisitor fv;
        MethodVisitor mv;

        cw.visit(V1_7, ACC_PUBLIC + ACC_FINAL + ACC_SUPER, n.token.getText(), null, "java/lang/Object", null);
        for (Definition.RecordField rf : n.fields) {
            fv = cw.visitField(ACC_PUBLIC + ACC_FINAL, rf.name, rf.type.getDescriptor(), null, null);
            fv.visitEnd();
        }

        mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(ILjava/lang/String;)V", null, null);
        mv.visitCode();
        mv.visitVarInsn(ALOAD, 0);
        mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V");

        int i = 1;
        for (Definition.RecordField rf : n.fields) {
            mv.visitVarInsn(ALOAD, 0);
            int op = rf.type.getOpcode(Opcodes.ILOAD);
            mv.visitVarInsn(op, i);
            mv.visitFieldInsn(PUTFIELD, n.token.getText(), rf.name, rf.type.getDescriptor());
            i++;
        }

        mv.visitInsn(RETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd();

        cw.visitEnd();

        byte b[] = cw.toByteArray();
        write(n.token.getText(), b);
        return null;
    }

    private void write(String name, byte b[]) {
        try {
            FileOutputStream fos = new FileOutputStream(name + ".class");
            fos.write(b);
            fos.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

	@Override
	public Void visit(UnitExpr n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Void visit(ExprSeq n) {
		for (Expr e : n.exprs)
			e.accept(this);
		return null;
	}
}
