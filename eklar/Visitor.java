package eklar;

import eklar.nodes.*;
import eklar.nodes.Expr.*;

public interface Visitor<T> {
    public T visit(NumExpr n);
    public T visit(Expr.ExprList n);
    public T visit(Binop n);
    public T visit(FloatExpr n);
    public T visit(StringExpr n);
    public T visit(CharExpr n);
    public T visit(ArrayExpr n);
    public T visit(ListExpr n);
    public T visit(TupleExpr n);
    public T visit(BoolExpr n);
    public T visit(Call n);
    public T visit(Func n);
    public T visit(IfExpr n);
    public T visit(Identifier n);
    public T visit(Println n);
    public T visit(ForExpr n);
    public T visit(Var n);
    public T visit(Binding n);
    public T visit(Param n);
    public T visit(LetExpr n);
    public T visit(Tuple n);
    public T visit(Match n);
    public T visit(Expr.Array n);
    public T visit(ArrayGet n);
    public T visit(ArraySet n);
    public T visit(Definition.Record n);
    public T visit(UnitExpr n);
    public T visit(ExprSeq n);
}
